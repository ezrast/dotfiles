# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
  # We have color support; assume it's compliant with Ecma-48
  # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
  # a case would tend to support setf rather than setaf.)
  if [ -n "$SSH_CONNECTION" ]; then
    color_prompt='37'
  else
    color_prompt='32'
  fi
else
  color_prompt=
fi

if [ -n "$color_prompt" ]; then
  PS1='\[\033['"$color_prompt"'m\]\u@\h\[\033[00m\] \t \[\033[01;34m\]\w\[\033[00m\]\$  '
else
  PS1='\u@\h \t \w\$ '
fi
unset color_prompt

# Set common aliases.
alias grep='grep --color=auto --exclude-dir=.git'
alias ls='ls -aF --color=auto'
alias diff='diff -U5'
alias code='code -n'

alias cp='cp -i'
alias rm='rm -i'
alias mv='mv -i'

# Restores old-style `ls` formatting.
export LC_COLLATE="C"
export QUOTING_STYLE="literal"

# Quit giving me nano.
export EDITOR=vim

# Generates a passphrase with mixed cases, a number, a symbol, and 77 bits of
# entropy that don't depend on any of those things.
function passphrase {
  echo '1!' `sort -R $HOME/eff_large_wordlist.txt | head -n6 | cut -f2 | sed 's/\b./\u&/g'`
}

# Make rbenv go.
if [ -x "$HOME/.rbenv/bin/rbenv" ]; then
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
fi

export PATH="$PATH:$HOME/opt/bin:$HOME/.local/bin"

# Allow for system-specific settings
if [ -f ~/.bash_local ]; then
    . ~/.bash_local
fi
