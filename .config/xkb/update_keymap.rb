# So here's the deal
# I made a partial keymap that works just fine if you save it as
# /usr/share/X11/xkb/symbols/ezrast and do `setxkbmap ezrast`.
# But after wasting hours with setxkbmap and xkbcomp I can't get it to work
# from a location that doesn't require root.
# So instead I wrote this script to hack my changes into a dump of the current
# X settings.
# It's stupid but it works.
# Usage: xkbcomp $DISPLAY - | ruby update_keymap.rb ~/.config/xkb/ezrast | xkbcomp - $DISPLAY

keys = File.read ARGV[0]
map = STDIN.read

keys.each_line do |line|
  if line =~ /^\s*key\s*<(\S+)>\s*{.*?};/
    map.gsub!(/key\s*<#{$1}>\s*{.*?};/m, line.strip)
  end
end

puts map
